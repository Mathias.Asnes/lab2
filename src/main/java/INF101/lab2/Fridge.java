package INF101.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    List<FridgeItem> fridgeContents = new ArrayList<>();
    
	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	public int nItemsInFridge() {
        return this.fridgeContents.size();
    }

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	public int totalSize() {
        return 20;
    }

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public boolean placeIn(FridgeItem item) {
		int n_items = nItemsInFridge();
		int tot_size = totalSize();
		if (n_items < tot_size) {
        	this.fridgeContents.add(item);
			return true;
		}
		else {
			return false;
		}
    }

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	public void takeOut(FridgeItem item) {
		if (this.fridgeContents.contains(item)) {
			this.fridgeContents.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
    }

	/**
	 * Remove all items from the fridge
	 */
	public void emptyFridge() {
		this.fridgeContents.clear();
    }

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredContents = new ArrayList<>();
		for (ListIterator<FridgeItem> iter = fridgeContents.listIterator(); iter.hasNext(); ) {
			FridgeItem item = iter.next();
			if (item.hasExpired()) {
				expiredContents.add(item);
				iter.remove();
			}
		}
			
		return expiredContents;
    }
}


